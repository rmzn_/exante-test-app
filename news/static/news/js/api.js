const API_BASE_URL = 'http://localhost:8080/api'
const CATEGORIES_API_URL = `${API_BASE_URL}/categories/`
const NEWS_API_URL = `${API_BASE_URL}/articles/`

const JSON_HEADERS = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

const FILE_UPLOAD_HEADERS = {
    'Content-Type': 'multipart/form-data',
}

function  fetchCategories () {
    return fetch(CATEGORIES_API_URL).then(response => response.json())
}

function fetchNews (url, categoryId) {
    let fetchUrl = url ? url : NEWS_API_URL;

    if (categoryId) {
        fetchUrl = fetchUrl + '?' + new URLSearchParams({ category: categoryId })
    }

    return fetch(fetchUrl).then(response => response.json())
}

function addCategory (name) {
    return fetch(CATEGORIES_API_URL, {
        method: 'post',
        headers: {
            ...JSON_HEADERS,
        },
        body: JSON.stringify({ name })
    }).then(response => response.json());
}

function addArticle ({ title, category, image, text }) {
    const data = new FormData();
    data.append('title', title)
    data.append('category', category)
    data.append('image', image)
    data.append('text', text)

    return fetch(NEWS_API_URL, {
        method: 'post',
        body: data
    }).then(response => response.json());
}