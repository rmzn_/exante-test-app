from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)

class Article(models.Model):
    title = models.CharField(max_length=200)
    text = models.CharField(max_length=1000)
    image = models.ImageField()
    added_date = models.DateField(auto_now_add=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)