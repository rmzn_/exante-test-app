# Generated by Django 3.0.6 on 2020-05-11 15:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0002_auto_20200508_2124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='added_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
