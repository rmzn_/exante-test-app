from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from rest_framework import routers
from news import views

router = routers.DefaultRouter()
router.register(r'categories', views.CategoryViewSet)
router.register(r'articles', views.ArticleViewSet)

urlpatterns = [
    path('', views.index),
    path('api/', include(router.urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)