FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /news_app
WORKDIR /news_app
ADD . /news_app/
RUN pip install -r requirements.txt
RUN python manage.py migrate
CMD python manage.py runserver localhost:8080